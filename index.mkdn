Test!
======

How about a list?
------------------
 - Test
 - Test

Here's some code:
-----------------
    #!/usr/bin/python
    # Gist-test by Dustin Ingram
    
    def main() :
        print "%s,%s" % ('Hello', 'World')
    
    if __name__ == "__main__" :
        main() :
[blah][1]

*test*

  [1]: http://gist.github.com/580123#file_gistfile1.py